""" Settings """

XML = False
JSON = True

MONGO_HOST = 'mongo'
MONGO_PORT = 27017

MONGO_DBNAME = 'frota'

DATE_FORMAT = "%d/%m/%Y %H:%M:%S %z"

RESOURCE_METHODS = [ 'GET', 'POST' ]
ITEM_METHODS = [ 'GET', 'PATCH', 'PUT' ]

veiculos = {
    'item_title' : 'veiculo',
    'additional_lookup' : {
        'url'   : 'regex( "[\w]+" )',
        'field' : 'placa'
    },
    'schema' : {
        'nome' : {
            'type' : 'string',
        },
        'placa' : {
            'type'     : 'string',
            'required' : True,
            'unique'   : True,
        },
        'rota' : {
            'type' : 'string'
        },
        'label' : {
            'type' : 'string'
        },
        'status' : {
            'type' : 'string'
        }
    },
}

rastreamento = {
    'item_title' : 'rastreamento',
    'schema' : {
        'motorista' : {
            'type' : 'dict',
            'schema' : {
                'matricula' : { 'type' : 'string' },
                'nome' : { 'type' : 'string' },
            },
        }, 
        'veiculo' : {
            'type' : 'objectid',
            'data_relation': {
                'resource'   : 'veiculos',
                'field'      : '_id',
                'embeddable' : True
            },
        },
        'data' : {
            'type' : 'datetime',
        },
        'localizacao' : {
            'type' : 'point',
        },
    },
}

DOMAIN = {
    'veiculos'     : veiculos,
    'rastreamento' : rastreamento,
}
