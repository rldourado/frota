""" App Entrypoint """

import ldap

from eve                                    import Eve
from eve.auth                               import BasicAuth

# from oauth2 import BearerAuth
# from flask.sessions.sentinel import ResourceOwnerPasswordCredentials, oauth


class MyBasicAuth( BasicAuth ):
    def check_auth( self, username, password, allowed_roles, resource, method ):
        print( '' )
        print( '--------------------------------------------------' )
        print( '|                    check_auth                  |' )
        print( '--------------------------------------------------' )
        print( 'username       : %s' % username )
        print( 'password       : %s' % password )
        print( 'allowed_roles  : %s' % allowed_roles )
        print( 'resource       : %s' % resource )
        print( 'method         : %s' % method )
        print( '--------------------------------------------------' )
        print( '' )

        if ( not username ) or ( not password ):
            return False

        AD_SERVER   = 'ldaps://10.114.171.40:636'
        AD_ADMIN    = 'cn=Adm Intranet,cn=Users,dc=MP-PE,dc=LOCAL'
        AD_PASSWORD = 'adm1020'
        AD_BASEDN   = 'dc=MP-PE,dc=LOCAL'

        resultado = self.verifica_usuario_ad( AD_SERVER, AD_ADMIN, AD_PASSWORD, AD_BASEDN, username, password )

        print( '--------------------------------------------------' )
        print( 'resultado      : %s' % resultado )
        print( '--------------------------------------------------' )

        return resultado

    def verifica_usuario_ad( self, server, admin, admin_password, basedn, username, password ):
        print( '' )
        print( '--------------------------------------------------' )
        print( '|               verifica_usuario_ad              |' )
        print( '--------------------------------------------------' )
        print( 'server         : %s' % server )
        print( 'admin          : %s' % admin )
        print( 'admin_password : %s' % admin_password )
        print( 'basedn         : %s' % basedn )
        print( 'username       : %s' % username )
        print( 'password       : %s' % password )
        print( '--------------------------------------------------' )
        print( '' )

        try:
            # Bind as admin to search for the user entry DN
            ldap.set_option( ldap.OPT_X_TLS_REQUIRE_CERT, 0 )
            anonymous_connection = ldap.initialize( server )
            anonymous_connection.set_option(ldap.OPT_REFERRALS, 0)
            anonymous_connection.simple_bind_s( admin, admin_password )
            filter  = 'sAMAccountName=' + username
            results = anonymous_connection.search_s( basedn, ldap.SCOPE_SUBTREE, filter )
            anonymous_connection.unbind_s()

            for dn, entry in results:
                dn = str( dn )
                if dn is not None:
                    userdn = dn
                    break

            # se conecta como usuário para validar senha atual
            ldap.set_option( ldap.OPT_X_TLS_REQUIRE_CERT, 0 )
            user_ad_connection = ldap.initialize( server )
            user_ad_connection.set_option(ldap.OPT_REFERRALS, 0)
            user_ad_connection.simple_bind_s( userdn, password )
            user_ad_connection.unbind_s()

            return True
        except ldap.INVALID_CREDENTIALS as e:
            print( u'Credenciais invalidas (AD - verifica_usuario_ad): ' + str( e ) )
            return False

        except ldap.UNWILLING_TO_PERFORM as e:
            print( u'Nova Senha invalida (AD - verifica_usuario_ad): ' + str( e ) )
            return False

        except ldap.LDAPError as e:
            print( u'Error connecting to AD server (verifica_usuario_ad): ' + str( e ) )
            return False


app = Eve()
# app = Eve( auth = MyBasicAuth )
# app = Eve(auth=BearerAuth)
# ResourceOwnerPasswordCredentials(app)

# @app.route('/endpoint')
# @oauth.require_oauth()
# def restricted_access():
#     return "You made it through and accessed the protected resource!"

if __name__ == '__main__':
    # app.run( host = '0.0.0.0', port = 5010, ssl_context = 'adhoc' )
    app.run( host = '0.0.0.0', port = 5010 )
