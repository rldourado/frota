
(function( $ )
{

	$( document ).ready( function()
	{
        console.log( "jQuery " + ( jQuery ? $().jquery : "NOT" ) + " loaded" )

        $( '.blog-featured' ).hide();

        $( '#form-login-remember' ).remove();
        $( 'ul.unstyled' ).remove();
        $( '#login-form' ).wrap( '<div class="form-center"></div>' );
        $( '<img class="mppelogo" src="/templates/frota/images/logomppefooter.png">' ).insertBefore( '#login-form' );

	})
})( jQuery );
