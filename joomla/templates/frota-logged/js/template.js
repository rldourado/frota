
(function ($) {

    $( document ).ready( function () {
        console.log("jQuery " + (jQuery ? $().jquery : "NOT") + " loaded")

        $( '#login-form' ).hide();

        $( 'iframe' ).load( function () {
            // var formLogin = $( '#login-form' ).detach();
            var formLoginGreeting = $( '#login-form' ).find( '.login-greeting' ).html();
            var formLoginAction   = $( '#login-form' ).attr( 'action' );
            var formLoginHiddens  = $( '#login-form' ).find( '[type=hidden]' );

            // $( 'iframe' ).contents().find( 'div.row.form-login' ).append( formLogin );
            
            $( 'iframe' ).contents().find( 'div.row.form-login form span.greeting' ).html( formLoginGreeting );
            $( 'iframe' ).contents().find( 'div.row.form-login form' ).attr( 'action', formLoginAction );
            $( 'iframe' ).contents().find( 'div.row.form-login form' ).append( formLoginHiddens );
        });

    })
})( jQuery );
