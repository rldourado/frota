FROM nginx:1.13.5

MAINTAINER Rubens Levy Dourado <rubens.dourado@gmail.com>
LABEL maintainer-name="Rubens Levy Dourado"
LABEL maintainer-mail="rubensd@mppe.mp.br"

ADD nginx.conf /etc/nginx/nginx.conf
