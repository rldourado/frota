""" App Entrypoint """

from eve import Eve
# from oauth2 import BearerAuth
# from flask.sessions.sentinel import ResourceOwnerPasswordCredentials, oauth

app = Eve()
# app = Eve(auth=BearerAuth)
# ResourceOwnerPasswordCredentials(app)

# @app.route('/endpoint')
# @oauth.require_oauth()
# def restricted_access():
#     return "You made it through and accessed the protected resource!"

if __name__ == '__main__':
    # app.run( host = '0.0.0.0', port = 5010, ssl_context = 'adhoc' )
    app.run( host = '0.0.0.0', port = 5010 )
