#!/bin/bash

set -o pipefail

IMAGE=eve
VERSION=0.1
# REGISTRY=64.137.244.184:5000

docker build -t ${IMAGE}:${VERSION} . | tee build.log || exit 1
ID=$(tail -1 build.log | awk '{print $3;}')

# docker tag ${IMAGE}:${VERSION} ${REGISTRY}/${IMAGE}:${VERSION}
# docker push ${REGISTRY}/${IMAGE}:${VERSION}

docker images | grep ${IMAGE}
