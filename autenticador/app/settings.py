""" Settings """

XML = False
JSON = True

MONGO_HOST = 'mongo'
MONGO_PORT = 27017

MONGO_DBNAME = 'frota'

RESOURCE_METHODS = [ 'GET', 'POST' ]
ITEM_METHODS = [ 'GET', 'PATCH', 'PUT' ]

veiculos = {
    'schema' : {
        'nome' : {
            'type' : 'string',
        },
        'placa' : {
            'type' : 'string',
        },
        'label' : {
            'type' : 'string'
        },
        'ativo' : {
            'type' : 'boolean'
        }
    },
}

rastreamento = {
    'schema' : {
        'veiculo' : {
            'type' : 'objectid',
            'data_relation': {
                'resource'   : 'veiculos',
                'field'      : '_id',
                'embeddable' : True
            },
        },
        'location' : {
            'type' : 'point',
        },
    },
}

DOMAIN = {
    'veiculos'     : veiculos,
    'rastreamento' : rastreamento,
}
