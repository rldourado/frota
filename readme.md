# Execução do Sistema (compilação e execução)

## Variável de Ambiente

``` bash
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_152.jdk/Contents/Home
export PROJECT_ROOT=/Users/rldourado/Work/frota
```

## Node

``` bash
npm install -g @angular/cli
```

## Redes Virtuais

``` bash
docker network create frota-backend
docker network create frota-frontend
```

## backend

``` bash
cd $PROJECT_ROOT/infra/docker/backend/
./build.sh
docker-compose up -d
```

## PHP

``` bash
cd $PROJECT_ROOT/infra/docker/php/
./build.sh
```

## frontend

``` bash
cd $PROJECT_ROOT/frontend/
npm install --update-binary --verbose
ng serve -dev -sm
ng build --base-href /frontend/

cd $PROJECT_ROOT/infra/docker/frontend/
./build.sh
docker-compose up -d
```

## Rastreador

``` bash
cd $PROJECT_ROOT/rastreador/
npm install --update-binary --verbose
react-native run-android && react-native log-android
```

### Build do APK

``` bash
cd $PROJECT_ROOT/rastreador/
cd android && ./gradlew assembleRelease && cd ..
mv android/app/build/outputs/apk/app-release.apk ./rastreador.apk
```
