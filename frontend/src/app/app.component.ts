import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { setInterval } from 'timers';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    title = '';

    timer: number = 30;

    lat: number = -8.055060127471394;
    lng: number = -34.88241255283356;
    zoom: number = 17;

    veiculos: veiculo[] = [];
    markers: marker[] = [];

    // mkr: marker = { lat: this.lat, lng: this.lng, label: 'A', draggable: false };
    // markers: marker[] = [
    //     { lat: -8.055060127471394, lng: -34.882412552833560, label: 'A', draggable: false },
    //     { lat: -8.054603338903588, lng: -34.883147478103640, label: 'A', draggable: false },
    //     { lat: -8.054231533874926, lng: -34.883742928504944, label: 'A', draggable: false },
    //     { lat: -8.053865040012534, lng: -34.884316921234130, label: 'A', draggable: false },
    //     { lat: -8.053450742203438, lng: -34.884231090545654, label: 'A', draggable: false },
    //     { lat: -8.052919590546079, lng: -34.883887767791750, label: 'A', draggable: false },
    //     { lat: -8.052452176511329, lng: -34.883614182472230, label: 'A', draggable: false },
    //     { lat: -8.051990073469389, lng: -34.883313775062560, label: 'A', draggable: false },
    //     { lat: -8.051597019892334, lng: -34.883067011833190, label: 'A', draggable: false },
    //     { lat: -8.051203965933853, lng: -34.882852435112000, label: 'A', draggable: false },
    //     { lat: -8.051267704439510, lng: -34.882562756538390, label: 'A', draggable: false },
    //     { lat: -8.051437673738924, lng: -34.882337450981140, label: 'A', draggable: false },
    //     { lat: -8.051724496769925, lng: -34.882428646087650, label: 'A', draggable: false },
    //     { lat: -8.051984761937105, lng: -34.882589578628540, label: 'A', draggable: false },
    //     { lat: -8.052229092350006, lng: -34.882755875587460, label: 'A', draggable: false },
    //     { lat: -8.052462799563557, lng: -34.882900714874270, label: 'A', draggable: false },
    //     { lat: -8.052685883596132, lng: -34.883077740669250, label: 'A', draggable: false },
    //     { lat: -8.052882409903976, lng: -34.883254766464230, label: 'A', draggable: false },
    //     { lat: -8.052638079885210, lng: -34.883576631546020, label: 'A', draggable: false },
    //     { lat: -8.052473422615519, lng: -34.883823394775390, label: 'A', draggable: false },
    //     { lat: -8.052239715408100, lng: -34.884102344512940, label: 'A', draggable: false },
    //     { lat: -8.051995385001598, lng: -34.884408116340640, label: 'A', draggable: false },
    //     { lat: -8.051921023544281, lng: -34.884606599807740, label: 'A', draggable: false },
    //     { lat: -8.052218469291644, lng: -34.884392023086550, label: 'A', draggable: false },
    //     { lat: -8.052484045667189, lng: -34.884075522422790, label: 'A', draggable: false },
    //     { lat: -8.052664637503100, lng: -34.883796572685240, label: 'A', draggable: false },
    //     { lat: -8.052861163821234, lng: -34.883517622947690, label: 'A', draggable: false },
    //     { lat: -8.053078936116442, lng: -34.883163571357730, label: 'A', draggable: false },
    //     { lat: -8.053259527687043, lng: -34.882852435112000, label: 'A', draggable: false },
    //     { lat: -8.053445430690326, lng: -34.882541298866270, label: 'A', draggable: false },
    //     { lat: -8.053610087564800, lng: -34.882278442382810, label: 'A', draggable: false },
    //     { lat: -8.053764121354503, lng: -34.882026314735410, label: 'A', draggable: false },
    //     { lat: -8.053934089606178, lng: -34.881790280342100, label: 'A', draggable: false },
    //     { lat: -8.054119992299752, lng: -34.881693720817566, label: 'A', draggable: false },
    //     { lat: -8.054300583406066, lng: -34.881833195686340, label: 'A', draggable: false },
    //     { lat: -8.054475862932225, lng: -34.881935119628906, label: '', draggable: false },
    //     { lat: -8.054709568849077, lng: -34.882058501243590, label: '', draggable: false },
    //     { lat: -8.054969832097695, lng: -34.882203340530396, label: '', draggable: false },
    //     { lat: -8.055224783689237, lng: -34.882342815399170, label: '', draggable: false },
    //     { lat: -8.055405374302770, lng: -34.882417917251590, label: '', draggable: false },
    //     { lat: -8.055670948588078, lng: -34.882541298866270, label: '', draggable: false },
    //     { lat: -8.056005571939517, lng: -34.882691502571106, label: '', draggable: false },
    //     { lat: -8.056393309762713, lng: -34.882798790931700, label: '', draggable: false },
    //     { lat: -8.056738555457080, lng: -34.882916808128360, label: '', draggable: false },
    //     { lat: -8.057089112322355, lng: -34.883088469505310, label: '', draggable: false },
    //     { lat: -8.057402488657026, lng: -34.883276224136350, label: '', draggable: false },
    //     { lat: -8.057673373089780, lng: -34.883340597152710, label: '', draggable: false },
    //     { lat: -8.057880519886607, lng: -34.883029460906980, label: '', draggable: false },
    //     { lat: -8.057944257341232, lng: -34.882476925849915, label: '', draggable: false },
    //     { lat: -8.058326681858132, lng: -34.881876111030580, label: '', draggable: false },
    //     { lat: -8.058682548792320, lng: -34.881323575973510, label: '', draggable: false },
    //     { lat: -8.058958743809120, lng: -34.880872964859010, label: '', draggable: false },
    //     { lat: -8.059314610187462, lng: -34.880298972129820, label: '', draggable: false },
    //     { lat: -8.059712967702401, lng: -34.879665970802310, label: '', draggable: false },
    //     { lat: -8.060148504803283, lng: -34.878963232040405, label: '', draggable: false },
    //     { lat: -8.060419387397818, lng: -34.878721833229065, label: '', draggable: false },
    //     { lat: -8.060844300906247, lng: -34.879097342491150, label: '', draggable: false },
    //     { lat: -8.061232034093262, lng: -34.879403114318850, label: '', draggable: false },
    //     { lat: -8.061550718626272, lng: -34.879698157310486, label: '', draggable: false },
    //     { lat: -8.061906582724752, lng: -34.879939556121826, label: '', draggable: false },
    //     { lat: -8.062134972951382, lng: -34.880073666572570, label: '', draggable: false },
    //     { lat: -8.062379297237097, lng: -34.879628419876100, label: '', draggable: false },
    //     { lat: -8.062257135112686, lng: -34.879408478736880, label: '', draggable: false },
    //     { lat: -8.061848157297218, lng: -34.879639148712160, label: '', draggable: false },
    //     { lat: -8.061407310617485, lng: -34.879494309425354, label: '', draggable: false },
    //     { lat: -8.061104560209769, lng: -34.879183173179630, label: '', draggable: false },
    //     { lat: -8.061216099860022, lng: -34.878539443016050, label: '', draggable: false },
    //     { lat: -8.061401999208770, lng: -34.877707958221436, label: '', draggable: false },
    //     { lat: -8.061502915962311, lng: -34.877192974090576, label: '', draggable: false },
    //     { lat: -8.062039367755927, lng: -34.877069592475890, label: '', draggable: false },
    //     { lat: -8.062697980866242, lng: -34.877005219459534, label: '', draggable: false },
    //     { lat: -8.063654030247090, lng: -34.876903295516970, label: '', draggable: false },
    //     { lat: -8.064450736338008, lng: -34.876817464828490, label: '', draggable: false },
    //     { lat: -8.065072165999280, lng: -34.876736998558044, label: '', draggable: false },
    //     { lat: -8.065088100080605, lng: -34.876495599746704, label: '', draggable: false },
    //     { lat: -8.064785352428139, lng: -34.876935482025150, label: '', draggable: false },
    //     { lat: -8.064981872859935, lng: -34.876318573951720, label: '', draggable: false },
    //     { lat: -8.064620700100928, lng: -34.876356124877930, label: '', draggable: false },
    //     { lat: -8.063563736791101, lng: -34.876415133476260, label: '', draggable: false },
    //     { lat: -8.063053843955382, lng: -34.876431226730350, label: '', draggable: false },
    //     { lat: -8.062910436479516, lng: -34.876747727394104, label: '', draggable: false },
    //     { lat: -8.062777651734300, lng: -34.877648949623110, label: '', draggable: false },
    //     { lat: -8.062650178337876, lng: -34.878507256507870, label: '', draggable: false },
    //     { lat: -8.062549261870560, lng: -34.879236817359924, label: '', draggable: false },
    //     { lat: -8.062336806067572, lng: -34.879424571990970, label: '', draggable: false },
    //     { lat: -8.061959696742454, lng: -34.879810810089110, label: '', draggable: false },
    //     { lat: -8.061874714310790, lng: -34.879580140113830, label: '', draggable: false },
    //     { lat: -8.061502915962311, lng: -34.879596233367920, label: '', draggable: false },
    //     { lat: -8.061120494447412, lng: -34.879269003868100, label: '', draggable: false },
    //     { lat: -8.061162985744690, lng: -34.878737926483154, label: '', draggable: false },
    //     { lat: -8.061274525378838, lng: -34.878180027008060, label: '', draggable: false },
    //     { lat: -8.060674335556433, lng: -34.877976179122925, label: '', draggable: false },
    //     { lat: -8.060313158950992, lng: -34.878410696983340, label: '', draggable: false },
    //     { lat: -8.059866999169714, lng: -34.879349470138550, label: '', draggable: false },
    //     { lat: -8.059346478803084, lng: -34.879853725433350, label: '', draggable: false },
    //     { lat: -8.058544451213233, lng: -34.879344105720520, label: '', draggable: false },
    //     { lat: -8.057503406408065, lng: -34.878727197647095, label: '', draggable: false },
    //     { lat: -8.057073177925979, lng: -34.879134893417360, label: '', draggable: false },
    //     { lat: -8.056372063864215, lng: -34.880256056785580, label: '', draggable: false },
    //     { lat: -8.055872784928411, lng: -34.881012439727780, label: '', draggable: false },
    //     { lat: -8.055453177686985, lng: -34.881677627563480, label: '', draggable: false },
    //     { lat: -8.055161045806408, lng: -34.882155060768130, label: '', draggable: false },
    //     { lat: -8.055012324040767, lng: -34.882380366325380, label: '', draggable: false }
    // ];

    constructor( private http: HttpClient ) { };

    ngOnInit() {

        // setInterval( function( this ) {
        //     console.log( 'setInterval' );
        //     this.getVeiculosOnline()
        // }, 1000 * 2 );
        this.getVeiculosOnline();
        this.startTimer();

        // this.simulate()

    };

    startTimer() {
        var that = this;
        setInterval( function() {
            that.timer = ( that.timer - 1 );
            if ( that.timer == 0 ) {
                that.getVeiculosOnline();
                that.timer = 30;
            }
        }, 1000 );
    }

    getVeiculosOnline() {
        this.http.get( 'api/veiculos?where={"status":{"$ne":"off-line"}}' ).subscribe( data => {
            console.group( 'getVeiculosOnline' );
            console.log( data );

            this.veiculos = [];
            this.markers = [];

            let veiculos: veiculo[] = data[ '_items' ];

            veiculos.forEach( ( veiculo, index ) => {
                console.log( veiculo );
                console.log( index );

                this.getUltimaPosicao( veiculo );
            });

            console.groupEnd();

            console.group( 'markers' );
            console.log( this.markers );
            console.groupEnd();
        });
    };

    getUltimaPosicao( veiculo: veiculo ) {
        this.http.get( 'api/rastreamento?where={"veiculo":"' + veiculo._id + '"}&sort=-_updated&max_results=1' ).subscribe( data => {
            console.group( 'getUltimaPosicao' );
            console.log( veiculo._id );
            console.log( data );

            // let dados = JSON.parse( data.toString() );
            
            console.log( data[ "_items" ] );
            console.log( data[ "_items" ][ 0 ] );
            
            let item = data[ "_items" ][ 0 ];

            if ( item ) {
                console.log( "item" );
                console.log( item.localizacao.coordinates );
                
                let mrkLabel: markerLabel = {
                    color: '#fff',
                    text: veiculo.label
                };

                let mrk: marker = {
                    lat       : item.localizacao.coordinates[ 0 ],
                    lng       : item.localizacao.coordinates[ 1 ],
                    label     : mrkLabel,
                    draggable : false,
                    iconUrl   : 'assets/marker-green.png'
                };

                var motorista: motorista = item.motorista;
                
                veiculo.marker = mrk;
                veiculo.motorista = motorista;

                this.veiculos.push( veiculo );
                this.markers.push( mrk );
            }

            console.groupEnd();
        });
    }

    centerMap( veiculo: veiculo ) {
        console.group( 'centerMap' );
        console.log( veiculo );

        this.lat = veiculo.marker.lat;
        this.lng = veiculo.marker.lng;

        console.groupEnd();
    }

    atualizar() {
        console.log( 'atualizar' );
        this.getVeiculosOnline();
        this.timer = 30;
    }

    // simulate() {
    //     var that = this;
    //     let i: number = 0;
    //     for ( let m of this.markers ) {
    //         i++;
    //         setTimeout( function timer() {
    //             that.mkr = m;
    //         }, i*3000 );
    //     }
    // };

}

// just an interface for type safety.
interface markerLabel {
    color        : string;
    fontFamily  ?: string;
    fontSize    ?: string;
    fontWeight  ?: string;
    text         : string;
}

interface marker {
    lat        : number;
    lng        : number;
    label     ?: markerLabel;
    draggable  : boolean;
    iconUrl   ?: string;
}

interface motorista {
    matricula : number;
    nome      : string;
}

interface veiculo {
    _id       : string;
    placa     : string;
    rota      : string;
    nome      : string;
    label     : string;
    status    : string;
    marker    : marker;
    motorista : motorista;
}
