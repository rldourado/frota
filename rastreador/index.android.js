/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Alert,
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    TextInput,
    Button,
    AsyncStorage
} from 'react-native';

import { FormLabel, FormInput } from 'react-native-elements';

import DeviceInfo from 'react-native-device-info';
import KeepAwake from 'react-native-keep-awake';
import MapView, { MAP_TYPES } from 'react-native-maps';
import { Col, Row, Grid } from 'react-native-easy-grid';

// import BatteryStatus from '@remobile/react-native-battery-status';
// import validation from 'validation'
// import validate from 'validation_wrapper'

const BACKEND_URL = 'http://van.mppe.mp.br/api';

const { width, height } = Dimensions.get('window')

const SCREEN_HEIGHT = height;
const SCREEN_WIDTH = width;
const ASPECT_RATIO = width / height;
const LATITUDE = -8.055060127471394;
const LONGITUDE = -34.88241255283356;
// const LATITUDE_DELTA = 0.0045;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const LATITUDE_DELTA = 0.0010;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

interface coordinate {
    lat: number;
    lng: number;
}

export default class rastreador extends Component {
    constructor( props ) {
        super( props );
    };

    state = {
        uniqueID: DeviceInfo.getUniqueID(),
        counterGeolocation: number = 0,
        accuracy: number = 50,
        region: {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        },
        startStopButton: {
            start: true,
            text: 'Iniciar',
            color: '#01DF01',
        },
        motorista: {
            matricula: null,
            placa: null,
            rota: null,
            nome: null,
            label: null,
        },
        motoristaNome: '',
        status: 'off-line',
        veiculo: null,
        battery: '',
        matriculaError: null,
    };

    // markers: marker[] = [
    //     { lat: -8.055060127471394, lng: -34.882412552833560, label: '', draggable: false },
    //     { lat: -8.054603338903588, lng: -34.883147478103640, label: '', draggable: false },
    //     { lat: -8.054231533874926, lng: -34.883742928504944, label: '', draggable: false },
    //     { lat: -8.053865040012534, lng: -34.884316921234130, label: '', draggable: false },
    //     { lat: -8.053450742203438, lng: -34.884231090545654, label: '', draggable: false },
    //     { lat: -8.052919590546079, lng: -34.883887767791750, label: '', draggable: false },
    //     { lat: -8.052452176511329, lng: -34.883614182472230, label: '', draggable: false },
    //     { lat: -8.051990073469389, lng: -34.883313775062560, label: '', draggable: false },
    //     { lat: -8.051597019892334, lng: -34.883067011833190, label: '', draggable: false },
    //     { lat: -8.051203965933853, lng: -34.882852435112000, label: '', draggable: false },
    //     { lat: -8.051267704439510, lng: -34.882562756538390, label: '', draggable: false },
    //     { lat: -8.051437673738924, lng: -34.882337450981140, label: '', draggable: false },
    //     { lat: -8.051724496769925, lng: -34.882428646087650, label: '', draggable: false },
    //     { lat: -8.051984761937105, lng: -34.882589578628540, label: '', draggable: false },
    //     { lat: -8.052229092350006, lng: -34.882755875587460, label: '', draggable: false },
    //     { lat: -8.052462799563557, lng: -34.882900714874270, label: '', draggable: false },
    //     { lat: -8.052685883596132, lng: -34.883077740669250, label: '', draggable: false },
    //     { lat: -8.052882409903976, lng: -34.883254766464230, label: '', draggable: false },
    //     { lat: -8.052638079885210, lng: -34.883576631546020, label: '', draggable: false },
    //     { lat: -8.052473422615519, lng: -34.883823394775390, label: '', draggable: false },
    //     { lat: -8.052239715408100, lng: -34.884102344512940, label: '', draggable: false },
    //     { lat: -8.051995385001598, lng: -34.884408116340640, label: '', draggable: false },
    //     { lat: -8.051921023544281, lng: -34.884606599807740, label: '', draggable: false },
    //     { lat: -8.052218469291644, lng: -34.884392023086550, label: '', draggable: false },
    //     { lat: -8.052484045667189, lng: -34.884075522422790, label: '', draggable: false },
    //     { lat: -8.052664637503100, lng: -34.883796572685240, label: '', draggable: false },
    //     { lat: -8.052861163821234, lng: -34.883517622947690, label: '', draggable: false },
    //     { lat: -8.053078936116442, lng: -34.883163571357730, label: '', draggable: false },
    //     { lat: -8.053259527687043, lng: -34.882852435112000, label: '', draggable: false },
    //     { lat: -8.053445430690326, lng: -34.882541298866270, label: '', draggable: false },
    //     { lat: -8.053610087564800, lng: -34.882278442382810, label: '', draggable: false },
    //     { lat: -8.053764121354503, lng: -34.882026314735410, label: '', draggable: false },
    //     { lat: -8.053934089606178, lng: -34.881790280342100, label: '', draggable: false },
    //     { lat: -8.054119992299752, lng: -34.881693720817566, label: '', draggable: false },
    //     { lat: -8.054300583406066, lng: -34.881833195686340, label: '', draggable: false },
    //     { lat: -8.054475862932225, lng: -34.881935119628906, label: '', draggable: false },
    //     { lat: -8.054709568849077, lng: -34.882058501243590, label: '', draggable: false },
    //     { lat: -8.054969832097695, lng: -34.882203340530396, label: '', draggable: false },
    //     { lat: -8.055224783689237, lng: -34.882342815399170, label: '', draggable: false },
    //     { lat: -8.055405374302770, lng: -34.882417917251590, label: '', draggable: false },
    //     { lat: -8.055670948588078, lng: -34.882541298866270, label: '', draggable: false },
    //     { lat: -8.056005571939517, lng: -34.882691502571106, label: '', draggable: false },
    //     { lat: -8.056393309762713, lng: -34.882798790931700, label: '', draggable: false },
    //     { lat: -8.056738555457080, lng: -34.882916808128360, label: '', draggable: false },
    //     { lat: -8.057089112322355, lng: -34.883088469505310, label: '', draggable: false },
    //     { lat: -8.057402488657026, lng: -34.883276224136350, label: '', draggable: false },
    //     { lat: -8.057673373089780, lng: -34.883340597152710, label: '', draggable: false },
    //     { lat: -8.057880519886607, lng: -34.883029460906980, label: '', draggable: false },
    //     { lat: -8.057944257341232, lng: -34.882476925849915, label: '', draggable: false },
    //     { lat: -8.058326681858132, lng: -34.881876111030580, label: '', draggable: false },
    //     { lat: -8.058682548792320, lng: -34.881323575973510, label: '', draggable: false },
    //     { lat: -8.058958743809120, lng: -34.880872964859010, label: '', draggable: false },
    //     { lat: -8.059314610187462, lng: -34.880298972129820, label: '', draggable: false },
    //     { lat: -8.059712967702401, lng: -34.879665970802310, label: '', draggable: false },
    //     { lat: -8.060148504803283, lng: -34.878963232040405, label: '', draggable: false },
    //     { lat: -8.060419387397818, lng: -34.878721833229065, label: '', draggable: false },
    //     { lat: -8.060844300906247, lng: -34.879097342491150, label: '', draggable: false },
    //     { lat: -8.061232034093262, lng: -34.879403114318850, label: '', draggable: false },
    //     { lat: -8.061550718626272, lng: -34.879698157310486, label: '', draggable: false },
    //     { lat: -8.061906582724752, lng: -34.879939556121826, label: '', draggable: false },
    //     { lat: -8.062134972951382, lng: -34.880073666572570, label: '', draggable: false },
    //     { lat: -8.062379297237097, lng: -34.879628419876100, label: '', draggable: false },
    //     { lat: -8.062257135112686, lng: -34.879408478736880, label: '', draggable: false },
    //     { lat: -8.061848157297218, lng: -34.879639148712160, label: '', draggable: false },
    //     { lat: -8.061407310617485, lng: -34.879494309425354, label: '', draggable: false },
    //     { lat: -8.061104560209769, lng: -34.879183173179630, label: '', draggable: false },
    //     { lat: -8.061216099860022, lng: -34.878539443016050, label: '', draggable: false },
    //     { lat: -8.061401999208770, lng: -34.877707958221436, label: '', draggable: false },
    //     { lat: -8.061502915962311, lng: -34.877192974090576, label: '', draggable: false },
    //     { lat: -8.062039367755927, lng: -34.877069592475890, label: '', draggable: false },
    //     { lat: -8.062697980866242, lng: -34.877005219459534, label: '', draggable: false },
    //     { lat: -8.063654030247090, lng: -34.876903295516970, label: '', draggable: false },
    //     { lat: -8.064450736338008, lng: -34.876817464828490, label: '', draggable: false },
    //     { lat: -8.065072165999280, lng: -34.876736998558044, label: '', draggable: false },
    //     { lat: -8.065088100080605, lng: -34.876495599746704, label: '', draggable: false },
    //     { lat: -8.064785352428139, lng: -34.876935482025150, label: '', draggable: false },
    //     { lat: -8.064981872859935, lng: -34.876318573951720, label: '', draggable: false },
    //     { lat: -8.064620700100928, lng: -34.876356124877930, label: '', draggable: false },
    //     { lat: -8.063563736791101, lng: -34.876415133476260, label: '', draggable: false },
    //     { lat: -8.063053843955382, lng: -34.876431226730350, label: '', draggable: false },
    //     { lat: -8.062910436479516, lng: -34.876747727394104, label: '', draggable: false },
    //     { lat: -8.062777651734300, lng: -34.877648949623110, label: '', draggable: false },
    //     { lat: -8.062650178337876, lng: -34.878507256507870, label: '', draggable: false },
    //     { lat: -8.062549261870560, lng: -34.879236817359924, label: '', draggable: false },
    //     { lat: -8.062336806067572, lng: -34.879424571990970, label: '', draggable: false },
    //     { lat: -8.061959696742454, lng: -34.879810810089110, label: '', draggable: false },
    //     { lat: -8.061874714310790, lng: -34.879580140113830, label: '', draggable: false },
    //     { lat: -8.061502915962311, lng: -34.879596233367920, label: '', draggable: false },
    //     { lat: -8.061120494447412, lng: -34.879269003868100, label: '', draggable: false },
    //     { lat: -8.061162985744690, lng: -34.878737926483154, label: '', draggable: false },
    //     { lat: -8.061274525378838, lng: -34.878180027008060, label: '', draggable: false },
    //     { lat: -8.060674335556433, lng: -34.877976179122925, label: '', draggable: false },
    //     { lat: -8.060313158950992, lng: -34.878410696983340, label: '', draggable: false },
    //     { lat: -8.059866999169714, lng: -34.879349470138550, label: '', draggable: false },
    //     { lat: -8.059346478803084, lng: -34.879853725433350, label: '', draggable: false },
    //     { lat: -8.058544451213233, lng: -34.879344105720520, label: '', draggable: false },
    //     { lat: -8.057503406408065, lng: -34.878727197647095, label: '', draggable: false },
    //     { lat: -8.057073177925979, lng: -34.879134893417360, label: '', draggable: false },
    //     { lat: -8.056372063864215, lng: -34.880256056785580, label: '', draggable: false },
    //     { lat: -8.055872784928411, lng: -34.881012439727780, label: '', draggable: false },
    //     { lat: -8.055453177686985, lng: -34.881677627563480, label: '', draggable: false },
    //     { lat: -8.055161045806408, lng: -34.882155060768130, label: '', draggable: false },
    //     { lat: -8.055012324040767, lng: -34.882380366325380, label: '', draggable: false }
    // ];

    // simulate() {
    //     var that = this;
    //     let i: number = 0;
    //     for ( let m of this.markers ) {
    //         i++;
    //         setTimeout( function timer() {
    //             let pos = { coords : { latitude : m.lat, longitude : m.lng, accuracy : 30 } };
    //             that.setRegion( pos );
    //         }, i*3000 );
    //     }
    // };

    watchID = null;

    componentDidMount() {
        console.log( 'componentDidMount' );
        
        // BatteryStatus.register( {
        //     onBatteryStatus   : this.onBatteryStatus,
        //     onBatteryLow      : this.onBatteryLow,
        //     onBatteryCritical : this.onBatteryCritical,
        // } );

        this.getCurrentDriver();
    


        // KeepAwake.activate();

        // this.getCurrentPosition();

        // this.watchPosition();
        // this.simulate();

        // this.watchID = navigator.geolocation.watchPosition((position) => {
        //     console.log( position );
        //     console.log( position.coords );
        //     console.log( position.coords.accuracy );

        //     if ( position.coords.accuracy < 30 ) {
        //         let lat = position.coords.latitude;
        //         let lng = position.coords.longitude;

        //         let lastRegion = {
        //             latitude: lat,
        //             longitude: lng,
        //             latitudeDelta: LATITUDE_DELTA,
        //             longitudeDelta: LONGITUDE_DELTA,
        //         };

        //         console.log( lastRegion );

        //         this.setState( { region : position } );
        //     };
        // });
    };

    componentWillUnmount() {
        console.log( 'componentWillUnmount' );

        this.setCurrentDriver();

        // BatteryStatus.unregister();

        // KeepAwake.deactivate();

        // navigator.geolocation.clearWatch(this.watchID);
    };

    getCurrentDriver = async () => {
        console.log( 'getCurrentDriver' );

        try {
            const savedMotorista = await AsyncStorage.getItem( '@frota:motorista' );
            if ( savedMotorista !== null ) {
                console.log( savedMotorista );
                this.setState( { motorista: JSON.parse( savedMotorista ) } );
                console.log( this.state );
            }
        } catch ( error ) {
            console.error( error );
        }

        console.log( '' );
    }

    setCurrentDriver = async () => {
        console.log( 'setCurrentDriver' );

        try {
            const savedMotorista = await AsyncStorage.setItem( '@frota:motorista', JSON.stringify( this.state.motorista ) );
        } catch ( error ) {
            console.error( error );
        }

        console.log( '' );
    }

    getMotoristaNome = async () => {
        let url = 'https://www.mppe.mp.br/novaintranet/api/servidores/servidor_lista/?id=' + this.state.motorista.matricula;
        
        console.log( 'getMotoristaNome' );
        console.log( 'url : ' + url )
        console.log( '' );

        return await fetch( url, {
            'method' : 'GET',
            headers : {
                'Accept'        : 'application/json',
                'Content-Type'  : 'application/json',
                'Cache-Control' : 'no-cache',
            },
            body : null
            }
        )
        .catch( error => {
            console.log( 'getMotoristaNome.catch' );
            console.log( error );
        } );
;
    }

    setStatus = async ( placa, rota, nome, label, status ) => {
        let url = '';

        await fetch( 'http://van.mppe.mp.br/frontend/api/veiculos/' + placa )
        .then( ( response ) => {
            if ( response.ok && response.status == 200 ) {
                let item = JSON.parse( response._bodyText );
                
                console.log( 'item' );
                console.log( item );

                this.setState( { veiculo : item } );
                this.updateVeiculo( item._id, item._etag, rota, nome, label, status );
            } else {
                this.createVeiculo( placa, rota, nome, label, status );
            }
        } )
        .catch( ( error ) => {
            console.error( error );
        } );
    }

    createVeiculo = async ( placa, rota, nome, label, status ) => {
        await fetch( 'http://van.mppe.mp.br/frontend/api/veiculos', {
            'method' : 'POST',
            headers : {
                'Accept'       : 'application/json',
                'Content-Type' : 'application/json',
            },
            body : JSON.stringify( {
                placa  : placa,
                rota   : rota,
                nome   : nome,
                label  : label,
                status : status,
            } )
        } )
        .then( ( response ) => {
            console.log( 'response' );
            console.log( response );

            this.setState( { status : status } );
        } )
        .catch( ( error ) => {
            console.error( error );
        } );
    }

    updateVeiculo = async ( id, etag, rota, nome, label, status ) => {
        await fetch( 'http://van.mppe.mp.br/frontend/api/veiculos/' + id, {
            'method' : 'PATCH',
            headers : {
                'Accept'       : 'application/json',
                'Content-Type' : 'application/json',
                'If-Match'     : etag,
            },
            body : JSON.stringify( {
                rota   : rota,
                nome   : nome,
                label  : label,
                status : status,
            } )
        } )
        .then( ( response ) => {
            console.log( 'response' );
            console.log( response );

            this.setState( { status : status } );
        } )
        .catch( ( error ) => {
            console.error( error );
        } );
    }
    
    registrarLocalizacao = async ( position ) => {
        console.log( 'registrarLocalizacao' );
        console.log( this.state );
        console.log( position );

        await fetch( 'http://van.mppe.mp.br/frontend/api/rastreamento', {
            'method' : 'POST',
            headers : {
                'Accept'       : 'application/json',
                'Content-Type' : 'application/json',
            },
            body : JSON.stringify( {
                motorista: { matricula: this.state.motorista.matricula, nome: this.state.motoristaNome },
                veiculo: this.state.veiculo._id,
                localizacao: { type: 'Point', coordinates: [ position.coords.latitude, position.coords.longitude ] }
            } )
        } )
        .then( ( response ) => {
            console.log( 'registrarLocalizacao.fetch.then' );
            console.log( response );
        } )
        .catch( ( error ) => {
            console.error( error );
        } );
    }

    getCurrentPosition() {
        console.log( 'getCurrentPosition' );

        navigator.geolocation.getCurrentPosition(
            ( position ) => {
                this.setRegion( position );
            },
            ( error ) => console.log( error.message ),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };

    watchPosition() {
        this.watchID = navigator.geolocation.watchPosition(
            ( position ) => {
                console.log( 'watchPosition' );
                console.log( position );

                this.registrarLocalizacao( position );
                this.setRegion( position );
                this.setState( { counterGeolocation: ( this.state.counterGeolocation + 1 ) } );
            },
            ( error ) => console.log( error.message ),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000, distanceFilter: 30 }
        );
    };

    setRegion( position ) {
        let lat = position.coords.latitude;
        let lng = position.coords.longitude;

        let initialPosition = {
            latitude: lat,
            longitude: lng,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        };

        this.setState( { accuracy : position.coords.accuracy, region : initialPosition } );
        // this.saveState();
    };

    async saveState() {

        try {
            let url = BACKEND_URL + '/';
            let response = await fetch( url );
            let responseJson = await response.json();
        } catch( error ) {
            console.log( error );
        };
    };

    onRegionChange( newRegion ) {
        console.log( 'newRegion' );
        console.log( newRegion );

        this.setState( { region: newRegion } );
    };

    onBatteryStatus( info ) {
        this.setState( { battery : JSON.stringify( { onBatteryStatus : info } ) } );
    };

    onBatteryLow( info ) {
        this.setState( { battery : JSON.stringify( { onBatteryLow : info } ) } );
    };

    onBatteryCritical( info ) {
        this.setState( { battery : JSON.stringify( { onBatteryCritical : info } ) } );
    };

    startStop() {
        console.log( 'startStop' );

        var newState = this.state.startStopButton;

        if ( this.state.startStopButton.start ) {
            newState.start = false;
            newState.text = 'Parar';
            newState.color = '#FF0000';

            this.start();
        } else {
            newState.start = true;
            newState.text = 'Iniciar';
            newState.color = '#01DF01';

            this.stop();
        }

        this.setState( newState )
    }

    start() {
        console.log( 'start' );
        this.getMotoristaNome()
            .then( ( response ) => {
                if ( response ) {
                    this.setState( { motoristaNome : JSON.parse( response._bodyText ).objects[0].nome } );
    
                } else {
                    this.setState( { motoristaNome : this.state.motorista.matricula } );
                };

                this.setStatus( this.state.motorista.placa, this.state.motorista.rota, this.state.motorista.nome, this.state.motorista.label, 'on-line' );
    
                this.setCurrentDriver();
                
                KeepAwake.activate();
                this.getCurrentPosition();
                this.watchPosition();
            } )
            .catch( error => {
                console.log( 'start.getMotoristaNome.catch' );
                console.log( error );
            } );
    }

    stop() {
        this.setStatus( this.state.motorista.placa, this.state.motorista.rota, this.state.motorista.nome, this.state.motorista.label, 'off-line' );

        KeepAwake.deactivate();
        
        navigator.geolocation.clearWatch( this.watchID );      
    }

    render() {
        return (
            <View style={ styles.container }>

                <MapView
                    provider={ this.props.provider }
                    ref={ref => { this.map = ref; }}
                    mapType={ MAP_TYPES.STANDARD }
                    style={ styles.map }
                    region={ this.state.region }
                    showsUserLocation={ true }
                    followsUserLocation={ true }
                    showsMyLocationButton={ true }
                    showsTraffic={ true }
                >
                    <MapView.Marker
                        coordinate={ this.state.region }
                    >
                        <View style={ [ styles.radius, { height : this.state.accuracy, width : this.state.accuracy, borderRadius : this.state.accuracy / 2 } ] }>
                            <View style={ styles.marker }>
                            </View>
                        </View>
                    </MapView.Marker>
                </MapView>

                <Grid style={ styles.grid }>
                    <Row style={{ width: 400 }}>
                        <Col size={100}>
                            <View style={ styles.bubble }>
                                <FormInput
                                    placeholder="Matrícula"
                                    onChangeText={ ( text ) => this.setState( { motorista : { matricula : text, placa : this.state.motorista.placa, rota : this.state.motorista.rota, nome : this.state.motorista.nome, label : this.state.motorista.label } } ) }
                                    // onBlur={ () => {
                                    //     this.setState( {
                                    //         matriculaError: validate( 'matricula', this.state.motorista.matricula )
                                    //     } )
                                    // } }
                                    // error={ this.state.matriculaError }
                                    value={ this.state.motorista.matricula }
                                    editable={ this.state.startStopButton.start }
                                    autoCapitalize="characters"
                                />
                            </View>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <View style={ styles.bubble }>
                                <FormInput
                                    placeholder="Placa"
                                    onChangeText={ ( text ) => this.setState( { motorista : { matricula : this.state.motorista.matricula, placa : text, rota : this.state.motorista.rota, nome : this.state.motorista.nome, label : this.state.motorista.label } } ) }
                                    value={ this.state.motorista.placa }
                                    editable={ this.state.startStopButton.start }
                                    autoCapitalize="characters"
                                />
                            </View>
                        </Col>
                        <Col>
                            <View style={ styles.bubble }>
                                <FormInput
                                    placeholder="Rota"
                                    onChangeText={ ( text ) => this.setState( { motorista : { matricula : this.state.motorista.matricula, placa : this.state.motorista.placa, rota : text, nome : this.state.motorista.nome, label : this.state.motorista.label } } ) }
                                    value={ this.state.motorista.rota }
                                    editable={ this.state.startStopButton.start }
                                    autoCapitalize="characters"
                                />
                            </View>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <View style={ styles.bubble }>
                                <FormInput
                                    placeholder="Nome"
                                    onChangeText={ ( text ) => this.setState( { motorista : { matricula : this.state.motorista.matricula, placa : this.state.motorista.placa, rota : this.state.motorista.rota, nome : text, label : this.state.motorista.label } } ) }
                                    value={ this.state.motorista.nome }
                                    editable={ this.state.startStopButton.start }
                                    autoCapitalize="characters"
                                />
                            </View>
                        </Col>
                        <Col>
                            <View style={ styles.bubble }>
                                <FormInput
                                    placeholder="Label"
                                    onChangeText={ ( text ) => this.setState( { motorista : { matricula : this.state.motorista.matricula, placa : this.state.motorista.placa, rota : this.state.motorista.rota, nome : this.state.motorista.nome, label : text } } ) }
                                    value={ this.state.motorista.label }
                                    editable={ this.state.startStopButton.start }
                                    autoCapitalize="characters"
                                />
                            </View>
                        </Col>
                    </Row>
                </Grid>


                <Button
                    onPress={ () => this.startStop() }
                    title={ this.state.startStopButton.text }
                    color={ this.state.startStopButton.color }
                    accessibilityLabel={ this.state.startStopButton.text }
                    style={ styles.button }
                />

                <Text>
                    <Text>UID: </Text>
                    { this.state.uniqueID }
                </Text>
                <Text>
                    <Text>Motorista: </Text>
                    { this.state.motoristaNome }
                </Text>
                <Text>
                    <Text>Status: </Text>
                    { this.state.status }
                </Text>
                <Text>
                    <Text>Battery: </Text>
                    { this.state.battery }
                </Text>

                {/* <Text>
                    <Text>Matrícula: </Text>
                    { this.state.motorista.matricula }
                </Text>
                <Text>
                    <Text>Placa: </Text>
                    { this.state.motorista.placa }
                </Text> */}

                {/* <View style={ styles.buttonContainer }>
                    <Text style={ [ styles.bubble, styles.latlng, { textAlign: 'center' } ] }>
                        <Text>Accuracy: </Text>
                        { this.state.accuracy }
                    </Text>
                </View>

                <Text>
                    <Text>Region: </Text>
                    { JSON.stringify( this.state.region ) }
                </Text>

                <View style={ styles.buttonContainer }>
                    <Text style={ [ styles.bubble, styles.latlng, { textAlign: 'center' } ] }>
                        { this.state.counterGeolocation }, { this.state.fetchUrl }
                    </Text>
                </View>

                <View style={ styles.buttonContainer }>
                    <TouchableOpacity
                        onPress={ () => this.getCurrentPosition() }
                        style={ [ styles.bubble, styles.button ] }
                    >
                        <Text style={ styles.buttonText }>Localizar</Text>
                    </TouchableOpacity>
                </View> */}

            </View>
        );
    };
};

// just an interface for type safety.
interface marker {
    lat: number;
    lng: number;
    label?: string;
    draggable: boolean;
}


rastreador.propTypes = {
    provider: MapView.ProviderPropType,
};

const styles = StyleSheet.create( {

    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },

    radius: {
        // height: 50,
        // width: 50,
        // borderRadius: 50 / 2,
        backgroundColor: 'rgba( 0, 122, 255, 0.1 )',
        borderWidth: 1,
        borderColor: 'rgba( 0, 112, 255, 0.3 )',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
    },
    marker: {
        height: 15,
        width: 15,
        borderRadius: 15 / 2,
        borderWidth: 2,
        borderColor: 'white',
        backgroundColor: '#007AFF',
        overflow: 'hidden',
    },

    bubble: {
        backgroundColor: 'rgba( 255, 255, 255, 0.7 )',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    latlng: {
        width: 350,
        alignItems: 'stretch',
    },
    button: {
        backgroundColor: 'rgba( 0, 112, 255, 0.9 )',
        width: 100,
        paddingHorizontal: 12,
        alignItems: 'center',
        marginHorizontal: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: 'transparent',
    },
    buttonText: {
        color: 'white',
    },
    
} );

AppRegistry.registerComponent( 'rastreador', () => rastreador );
