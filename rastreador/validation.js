const validation = {
    matricula : {
        presence : {
            message: '^Informe a matrícula',
        },
        length : {
            is : 7,
            message : '^Matrícula deve conter 7 dígitos',
        },
        numericality : {
            onlyInteger: true,
        }
    },
};

export default validation