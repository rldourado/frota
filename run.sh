#!/bin/bash

comando=$*

docker network create -d bridge frota

if [[ -n "$comando" ]]; then
    #Backend
    docker run -d --rm --name mongo    --network="frota" -v `pwd`/mongo/data/db:/data/db -p 27017:27017 mongo:3.4.9
    docker run -d --rm --name eve      --network="frota" -v `pwd`/backend/app:/app -p 5010:5010 --link mongo:mongo -e 'PYTHONUNBUFFERED=0' frota/eve:0.7.4 python run.py
    #Frontend
    docker run -d --rm --name frontend --network="frota" -v `pwd`/backend/app:/app -v `pwd`/infra/etc:/etc/nginx/conf.d:ro -v `pwd`/infra/ssl:/etc/nginx/ssl:ro -v `pwd`/frontend/build:/frota/frontend -v `pwd`/infra/logs:/frota/logs -p 80:80 -p 443:443 frota-frontend:0.1 $comando
else
    #Backend
    docker run -d --rm --name mongo    --network="frota" -v `pwd`/mongo/data/db:/data/db -p 27017:27017 mongo:3.4.9
    docker run -d --rm --name eve      --network="frota" -v `pwd`/backend/app:/app -p 5010:5010 --link mongo:mongo -e 'PYTHONUNBUFFERED=0' frota/eve:0.7.4 python run.py
    #Frontend
    docker run -d --rm --name frontend --network="frota" -v `pwd`/backend/app:/app -v `pwd`/infra/etc:/etc/nginx/conf.d:ro -v `pwd`/infra/ssl:/etc/nginx/ssl:ro -v `pwd`/frontend/build:/frota/frontend -v `pwd`/infra/logs:/frota/logs -p 80:80 -p 443:443 frota-frontend:0.1
fi
