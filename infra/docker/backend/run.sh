#!/bin/bash

comando=$*

if [[ -n "$comando" ]]; then
    docker run -d --rm --name mongo -v `pwd`/data/db:/data/db -p 27017:27017 mongo:3.4.9
    docker run -d --rm --name eve -v `pwd`/app:/app -p 5010:5010 --link mongo:mongo -e 'PYTHONUNBUFFERED=0' eve:0.7.4 $comando
else
    docker run -d --rm --name mongo -v `pwd`/data/db:/data/db -p 27017:27017 mongo:3.4.9
    docker run -d --rm --name eve -v `pwd`/app:/app -p 5010:5010 --link mongo:mongo -e 'PYTHONUNBUFFERED=0' eve:0.7.4 python run.py
fi
