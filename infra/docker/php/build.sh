#!/bin/bash

set -o pipefail

IMAGE=frota/php
VERSION=7.1.10-fpm-jessie
# REGISTRY=10.114.170.30:5000

docker build -t ${IMAGE}:${VERSION} . | tee build.log || exit 1
ID=$(tail -1 build.log | awk '{print $3;}')

# docker tag ${IMAGE}:${VERSION} ${REGISTRY}/${IMAGE}:${VERSION}
# docker push ${REGISTRY}/${IMAGE}:${VERSION}

docker images | grep ${IMAGE}:${VERSION}
