var listaController = function( $scope, $http, $log, Session, CITSMART_TIPOS ) {

    $scope.situacoes = {
        1 : 'Em Andamento'   ,
        2 : 'Suspensa'       ,
        3 : 'Cancelada'      ,
        4 : 'Resolvida'      ,
        5 : 'Reaberta'       ,
        6 : 'Fechada'        ,
        7 : 'Re-Classificada'
    };

    $scope.tipos = CITSMART_TIPOS;

    var getRequestsByUserLogin = function() {
        var data = {
        	"sessionID"  : Session.id,
        	"queryName"  : "REQUESTS_BY_USER_LOGIN",
        	"parameters" : {
        		"login" : Session.username
        	}
        }

        $http
            .post(
                '/citsmart/services/data/query',
                data
            )
            .then(
                function( response ) {
                    $scope.requests = response.data.result;
                },
                function( response ) {
                    return false;
                }
            )
        ;

    };

    getRequestsByUserLogin();
};

listaController.$inject = [ '$scope', '$http', '$log', 'Session', 'CITSMART_TIPOS' ];

module.exports = listaController;
