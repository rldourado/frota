var menuController = function( $scope, $rootScope, AUTH_EVENTS, AuthService, Session ) {

    $scope.session = Session;

    $scope.logout = function() {
        AuthService.logout();
        $rootScope.$broadcast( AUTH_EVENTS.logoutSuccess );
    };

};

menuController.$inject = [ '$scope', '$rootScope', 'AUTH_EVENTS', 'AuthService', 'Session' ];

module.exports = menuController;
