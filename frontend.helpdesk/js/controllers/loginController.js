var loginController = function( $scope, $rootScope, AUTH_EVENTS, AuthService ) {
    $scope.credentials = {
        username : '',
        password : ''
    };

    $scope.login = function( credentials ) {
        AuthService
            .login( credentials )
            .then(
                function( sessionID ) {
                    if ( sessionID ) {
                        $rootScope.$broadcast( AUTH_EVENTS.loginSuccess );
                    };
                }, function () {
                    $rootScope.$broadcast( AUTH_EVENTS.loginFailed );
                }
            );
    };
};

loginController.$inject = [ '$scope', '$rootScope', 'AUTH_EVENTS', 'AuthService' ];

module.exports = loginController;
