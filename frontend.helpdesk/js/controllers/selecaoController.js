var selecaoController = function( $scope, $http, $log, Session ) {

    var getServiceCatalog = function() {
        var data = {
        	"sessionID" : Session.id,
        	"queryName" : "SERVICE_CATALOG"
        };

        $http
            .post(
                '/citsmart/services/data/query',
                data
            )
            .then(
                function( response ) {
                    $scope.services = response.data.result;
                },
                function( response ) {
                    return false;
                }
            )
        ;

    };

    var applyAccordion = function() {
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].onclick = function(){
                this.classList.toggle("active");
                this.nextElementSibling.classList.toggle("show");
            };
        }
    };

    $scope.showHide = function( that ) {
        that.classList.toggle( "active" );
        that.nextElementSibling.classList.toggle( "show" );
    };

    getServiceCatalog();
    applyAccordion();
};

selecaoController.$inject = [ '$scope', '$http', '$log', 'Session' ];

module.exports = selecaoController;
