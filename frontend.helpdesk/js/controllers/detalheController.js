var detalheController = function( $scope, $http, $log, $stateParams, Session, CITSMART_TIPOS ) {

    $scope.listRequest = $stateParams.request;
    $scope.tipos = CITSMART_TIPOS;

    var getRequestById = function( id ) {
        var data = {
        	"sessionID" : Session.id,
        	"number"    : id
        };

        $http
            .post(
                '/citsmart/services/request/getById',
                data
            )
            .then(
                function( response ) {
                    $scope.request = response.data.request;
                },
                function( response ) {
                    return false;
                }
            )
        ;
    };

    var listOccurrences = function( id ) {

        var data = {
        	"sessionID"     : Session.id,
        	"requestNumber" : id
        };

        $http
            .post(
                '/citsmart/services/request/listOccurrences',
                data
            )
            .then(
                function( response ) {
                    $scope.occurrences = response.data.occurrences;
                },
                function( response ) {
                    return false;
                }
            )
        ;
    };

    getRequestById( $scope.listRequest.number );
    listOccurrences( $scope.listRequest.number );
};

detalheController.$inject = [ '$scope', '$http', '$log', '$stateParams', 'Session', 'CITSMART_TIPOS' ];

module.exports = detalheController;
