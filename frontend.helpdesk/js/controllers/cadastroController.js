var cadastroController = function( $scope, $state, $stateParams, $http, $log, Session, uuid2, swal ) {

    $scope.parentService = $stateParams.service;

    $scope.localidadePlaceholder = '(Selecione uma unidade primeiro e depois uma localidade física)';

    var getUnidades = function() {
        var data = {
        	"sessionID"  : Session.id,
        	"queryName"  : "UNITS"
        };

        $http
            .post(
                '/citsmart/services/data/query',
                data
            )
            .then(
                function( response ) {
                    $scope.units = response.data.result;
                },
                function( response ) {
                    return false;
                }
            )
        ;
    };

    var getLocalidades = function( unit ) {
        var data = {
        	"sessionID"  : Session.id,
        	"queryName"  : "LOCALS_BY_UNIT_ID",
        	"parameters" : {
        		"id"   : unit.id
        	}
        };

        $http
            .post(
                '/citsmart/services/data/query',
                data
            )
            .then(
                function( response ) {
                    $scope.locals = response.data.result;
                    if ( $scope.locals.length > 0 ) {
                        $scope.localidadePlaceholder = '(Selecione uma localidade física)';
                    } else {
                        $scope.localidadePlaceholder = 'Nenhuma Localidade Física Cadastrada Para a Unidade Selecionada';
                    }
                },
                function( response ) {
                    return false;
                }
            )
        ;
    };

    var getServicesByParentAndType = function( parent_id, type ) {
        var data = {
        	"sessionID"  : Session.id,
        	"queryName"  : "SERVICES_BY_PARENT",
        	"parameters" : {
        		"id"   : parent_id,
        		"tipo" : type
        	}
        };

        $http
            .post(
                '/citsmart/services/data/query',
                data
            )
            .then(
                function( response ) {
                    $scope.services = response.data.result;
                },
                function( response ) {
                    return false;
                }
            )
        ;
    };

    $scope.typeSelection = function( parent_id, type ) {
        getServicesByParentAndType( parent_id, type );
    };

    $scope.unitSelection = function( unit ) {
        if ( unit ) {
            getLocalidades( unit );
        } else {
            $scope.localidadePlaceholder = '(Selecione uma unidade primeiro e depois uma localidade física)';
            $scope.locals = [];
        }
    };

    $scope.save = function( request ) {
        var description = "";

        if ( request.tombo ) {
            description += "TOMBO: " + request.tombo + "<br>";
        }

        if ( request.unit ) {
            description += "UNIDADE: " + request.unit.name + "<br>";
        }

        if ( request.local ) {
            description += "LOCAL: " + request.local.name + "<br>";
        }

        if ( description ) {
            description += "<br>";
        }

        description += request.description;

        var data = {
        	"sessionID" : Session.id,
            "synchronize"   : "false",
        	"sourceRequest" : {
        		"numberOrigin"  : uuid2.newNumberOrigin(),
        		"type"          : request.type,
        		"description"   : description,
        		"userID"        : Session.username,
        		"contact"       : {
        			"name"          : Session.username,
        			"email"         : Session.username + "@mppe.mp.br",
        			"phoneNumber"   : request.phone
        		},
        		"contractID"    : "2",
        		"service"       : {
        			"code"          : request.service.id,
        			"name"          : request.service.name
        		}
        	}
        };

        $http
            .post(
                '/citsmart/services/request/create',
                data
            )
            .then(
                function( response ) {
                    swal( 'Chamado cadastrado com sucesso', response.data.request.number, 'success' );
                    $state.go( 'root.lista' );
                },
                function( response ) {
                    return false;
                }
            )
        ;

    };

    getUnidades();

};

cadastroController.$inject = [ '$scope', '$state', '$stateParams', '$http', '$log', 'Session', 'uuid2', 'swal' ];

module.exports = cadastroController;
