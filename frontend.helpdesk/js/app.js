/*jshint strict:false */
'use strict';

var window  = window;
var require = require;
var angular = require( 'angular' );

require( 'angular-messages' );
require( 'angular-sanitize' );
require( 'angular-ui-router' );
require( 'angular-ui-bootstrap' );
require( 'angular-loading-bar' );
require( 'angular-animate' );
require( 'angular-input-masks' );
require( 'angular-filter' );
require( 'ng-infinite-scroll' );
require( 'moment' );
require( 'moment/locale/pt-br' );
require( 'moment-timezone' );
require( 'angular-moment' );
require( 'sweetalert2' );

// require( '../bower_components/ngInfiniteScroll/build/ng-infinite-scroll' );
require( '../bower_components/ngprogress/build/ngprogress' );

/* @ngInject */

var loginController       = require( './controllers/loginController' );
var menuController        = require( './controllers/menuController' );
var listaController       = require( './controllers/listaController' );
var detalheController     = require( './controllers/detalheController' );
var selecaoController     = require( './controllers/selecaoController' );
var cadastroController    = require( './controllers/cadastroController' );

var app = angular.module( 'app', [ 'ui.router', 'ui.bootstrap', 'infinite-scroll', 'angular-loading-bar', 'ngAnimate', 'angularMoment', 'ui.utils.masks', 'ngMessages', 'ngSanitize', 'angular.filter' ] );

app.constant( 'moment', require( 'moment-timezone' ) );

app.constant( 'swal', require( 'sweetalert2' ) );

/*
* Techniques for authentication in AngularJS applications
* https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec#.3gdg1dsqt
*/
app.constant( 'AUTH_EVENTS', {
    loginSuccess     : 'auth-login-success',
    loginFailed      : 'auth-login-failed',
    logoutSuccess    : 'auth-logout-success',
    sessionTimeout   : 'auth-session-timeout',
    notAuthenticated : 'auth-not-authenticated',
    notAuthorized    : 'auth-not-authorized'
} );

app.constant( 'CITSMART_TIPOS', {
    'I' : 'Incidente',
    'R' : 'Requisição'
} );

app.factory( 'AuthService', function( $http, Session ) {
    var authService = {};

    authService.login = function( credentials ) {
        var data = {
            "userName" : credentials.username,
            "password" : credentials.password,
            "platform" : "helpdesk"
        };

        return $http
            .post(
                '/citsmart/services/login',
                data
            )
            .then(
                function( response ) {
                    Session.create( response.data.sessionID, credentials.username );

                    var data = {
                    	"sessionID"  : response.data.sessionID,
                    	"queryName"  : "USER",
                    	"parameters" : {
                    		"login" : credentials.username
                    	}
                    };

                    $http
                        .post(
                            '/citsmart/services/data/query',
                            data
                        )
                        .then(
                            function( response ) {
                                Session.setName( response.data.result[ 0 ].nome );
                            },
                            function( response ) {
                                return false;
                            }
                        );

                    return response.data.sessionID;
                },
                function( response ) {
                    return false;
                }
            );
    };

    authService.logout = function() {
        Session.destroy();
    };

    authService.isAuthenticated = function () {
        return !!Session.id;
    };

    return authService;
} );

app.factory( 'AuthInterceptor', function( $rootScope, $q, AUTH_EVENTS ) {
    return {
        responseError : function( response ) {
            $rootScope.$broadcast(
                {
                    401: AUTH_EVENTS.notAuthenticated,
                    403: AUTH_EVENTS.notAuthorized,
                    412: AUTH_EVENTS.loginFailed,
                    419: AUTH_EVENTS.sessionTimeout,
                    440: AUTH_EVENTS.sessionTimeout
                }[ response.status ], response );

            return $q.reject( response );
        }
    };
} );

app.service( 'Session', function () {
    this.create = function( sessionId, username ) {
        this.id    = sessionId;
        this.username = username;
    };

    this.setName = function( name ) {
        this.name = name;
    };

    this.destroy = function () {
        this.id    = null;
        this.username = null;
    };
} );

app.factory( 'uuid2', [ function() {
        var s4 = function() {
            return Math.floor( ( 1 + Math.random() ) * 0x10000 )
                .toString( 16 )
                .substring( 1 );
        };

        return {

            newUuid: function() {
                // http://www.ietf.org/rfc/rfc4122.txt
                var s = [];
                var hexDigits = "0123456789abcdef";
                for (var i = 0; i < 36; i++) {
                    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
                }
                s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
                s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
                s[8] = s[13] = s[18] = s[23] = "-";
                return s.join("");
            },
            newGuid: function() {
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                    s4() + '-' + s4() + s4() + s4();
            },
            newNumberOrigin : function() {
                return s4() + s4() + s4() + s4() + s4() + s4();
            }
        };
} ] );

app.config( [ '$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', function( $locationProvider, $stateProvider, $urlRouterProvider, $httpProvider ) {

    $urlRouterProvider.otherwise( '/' );

    $stateProvider

        .state( 'login', {
            url : '/login',
            views : {
                'container@'   : {
                    templateUrl : 'partials/login.html',
                    controller  : loginController,
                },
            },
            data : {
                authenticate : false
            }
        } )

        .state( 'root', {
            url      : '',
            abstract : true,
            views : {

                'header@' : {
                    templateUrl : 'partials/header.html',
                },

                'menu@' : {
                    templateUrl : 'partials/menu.html',
                    controller  : menuController,
                },

                'footer@' : {
                    templateUrl : 'partials/footer.html',
                },

            },
            data : {
                authenticate : true
            }
        } )

        .state( 'root.lista', {
            url      : '/',
            views : {
                'container@'   : {
                    templateUrl : 'partials/lista.html',
                    controller  : listaController,
                },
            },
        } )

        .state( 'root.detalhe', {
            url    : '/detalhe',
            views  : {
                'container@'   : {
                    templateUrl : 'partials/detalhe.html',
                    controller  : detalheController,
                    // resolve     : {
                    //                 id : [ '$stateParams', function( $stateParams ) {
                    //                 return $stateParams.id; } ]
                    // },
                },
            },
            params : {
                request : null,
            },
        } )

        .state( 'root.selecao', {
            url      : '/selecao',
            views : {
                'container@'   : {
                    templateUrl : 'partials/selecao.html',
                    controller  : selecaoController,
                },
            },
        } )


        .state( 'root.cadastro', {
            url    : '/cadastro',
            views  : {
                'container@'   : {
                    templateUrl : 'partials/cadastro.html',
                    controller  : cadastroController,
                    // resolve     : {
                    //                 service_id : [ '$stateParams', function( $stateParams ) {
                    //                 return $stateParams.service_id; } ]
                    // }
                },
            },
            params : {
                service : null,
            },
        } )

    ;

    $httpProvider.interceptors.push( [ '$injector', function( $injector ) {
        return $injector.get( 'AuthInterceptor' );
    } ] );

} ] ); // fecha app.config()

app.filter( 'escapeURL', function() {
    return window.encodeURIComponent;
} );

app.run( [ '$rootScope', '$state', '$stateParams','AUTH_EVENTS', 'AuthService', 'swal', function( $rootScope, $state, $stateParams, AUTH_EVENTS, AuthService, swal ) {

    $rootScope.$on( '$stateChangeStart', function( event, toState, toParams, fromState, fromParams ) {
        if ( toState.data.authenticate && !AuthService.isAuthenticated() ) {
        // if ( !AuthService.isAuthenticated() ) {
            event.preventDefault();
            $rootScope.$broadcast( AUTH_EVENTS.notAuthenticated );
        }
    } );

    $rootScope.$on( AUTH_EVENTS.notAuthenticated, function( event ) {
        $state.go( 'login' );
    } );

    $rootScope.$on( AUTH_EVENTS.loginFailed, function( event, response ) {
        var descricao = response.data.error.description;

        swal( 'Falha ao Logar', descricao, 'error' );
        $state.go( 'login' );
    } );

    $rootScope.$on( AUTH_EVENTS.loginSuccess, function( event ) {
        $state.go( 'root.lista' );
    } );

    $rootScope.$on( AUTH_EVENTS.logoutSuccess, function( event ) {
        $state.go( 'login' );
    } );

    $rootScope.navigate = function( $event, to, params ) {

        // If the command key is down, open the URL in a new tab
        if ( $event.metaKey ) {
            var url = $state.href( to, params, { absolute : true } );
            window.open( url, '_blank' );
        } else {
            $state.go( to, params );
        }

    };

} ] );
