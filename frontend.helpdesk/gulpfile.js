'use strict';

var require = require;
// var console = console;

var gulp        = require( 'gulp' ),
    uglify      = require( 'gulp-uglify' ),
    ngAnnotate  = require( 'gulp-ng-annotate' ),
    inject      = require( 'gulp-inject' ),
    rev         = require( 'gulp-rev' ),
    streamify   = require( 'gulp-streamify' ),
    htmlmin     = require( 'gulp-htmlmin' ),
    browserify  = require( 'browserify' ),
    source      = require( 'vinyl-source-stream' ),
    buffer      = require( 'vinyl-buffer' ),
    es          = require( 'event-stream' ),
    del         = require( 'del' ),
    clean       = require( 'gulp-clean' ),
    rm          = require( 'gulp-rm' ),
    browserSync = require( 'browser-sync' ),
    webserver   = require( 'gulp-webserver' ),
    plumber     = require( 'gulp-plumber' );

gulp.task( 'default', [ 'dev' ] );

gulp.task( 'dev', [ 'clean-build', 'minify-html', 'minify-assets', 'minify-fonts', 'minify-img', 'browserify-dev', 'watch' ], function() {

    console.log( '\n Task DEV \n' );

} );

gulp.task( 'prod', [ 'clean-build', 'minify-html', 'minify-assets', 'minify-fonts', 'minify-img', 'browserify-prod' ], function() {

    console.log( '\n Task PROD \n' );

} );

gulp.task( 'clean-build', function () {
    return gulp.src( 'build/**/*', { read : false } )
        .pipe( rm() );
} );

gulp.task( 'browserify-dev', [ 'clean-build' ], function() {
    // var paths = del.sync(
    //     [ './build/**/*' ],
    //     {
    //         force  : false,
    //         dryRun : false
    //     }
    // );

    // console.log( 'Files and folders that was deleted:\n', paths.join( '\n' ) );

    var fontStream = gulp.src( [ './fonts/**/*' ] )
        .pipe( gulp.dest( './build/fonts/' ) );

    var cssStream = gulp.src( [ './css/**/*.css' ] )
        .pipe( gulp.dest( './build/css/' ) );

    var jsStream = browserify( './js/app.js' )
        .bundle()
        .pipe( plumber() )
        .pipe( source( './bundle.js' ) )
        .pipe( streamify( rev() ) )
        .pipe( gulp.dest( './build/js/' ) );

    return gulp.src( 'index.html' )
        .pipe( inject( es.merge( fontStream, cssStream, jsStream ), { relative : true, ignorePath : 'build' } ) )
        .pipe( gulp.dest( './build/' ) );
} );

gulp.task( 'browserify-prod', function() {
    var cssStream = gulp.src( [ './css/**/*.css' ] )
        .pipe( gulp.dest( './build/' ) );

    var jsStream = browserify( './js/app.js' )
        .bundle()
        .pipe( plumber() )
        .pipe( source( './bundle.js' ) )
        .pipe( streamify( ngAnnotate() ) )
        .pipe( streamify( uglify( { mangle : true } ) ) )
        .pipe( streamify( rev() ) )
        .pipe( gulp.dest( './build/' ) );

    return gulp.src( 'index.html' )
        .pipe( inject( es.merge( cssStream, jsStream ), { relative : true, ignorePath : 'build' } ) )
        .pipe( htmlmin( { collapseWhitespace : true } ) )
        .pipe( gulp.dest( './build/' ) );
} );

gulp.task( 'minify-html', [ 'clean-build' ], function() {
  return gulp.src( 'partials/**/*.html' )
    .pipe( htmlmin( { collapseWhitespace : true } ) )
    .pipe( gulp.dest( './build/partials/' ) );
} );

gulp.task( 'minify-assets', [ 'clean-build' ], function() {
  return gulp.src( './assets/**/*.*' )
    .pipe( gulp.dest( './build/assets/' ) );
} );

gulp.task( 'minify-img', [ 'clean-build' ], function() {
  return gulp.src( [ './img/**/*.jpg', './img/**/*.gif', './img/**/*.png' ] )
    .pipe( gulp.dest( './build/img/' ) );
} );

gulp.task( 'minify-fonts', [ 'clean-build' ], function() {
    return gulp.src( './fonts/**/*' )
      .pipe( gulp.dest( './build/fonts/' ) );
} );

gulp.task( 'clean', function() {
    return del(
        [ './build/**' ],
        {
            force  : true,
            dryRun : false
        }
    ).then( paths => {
        console.log( 'Files and folders that would be deleted:\n', paths.join( '\n' ) );
    } );
} );

gulp.task( 'browser-sync', function() {
    browserSync.init( {
        notify: false,
        port: 6543,
        ui: {
            port: 8080,
            weinre: {
                port: 9090
            }
        },
        server: {
            baseDir: './build',
            directory: true,
            index: 'index.htm',
            middleware: function( req, res, next ) {
                res.setHeader( 'Access-Control-Allow-Origin', 'http://10.114.171.52:8080' );
                next();
            }
        }
    } );
} );

gulp.task( 'webserver', function() {
    gulp.src( 'build' )
        .pipe( webserver( {
            host: '0.0.0.0',
            port: 8080,
            livereload: false,
            directoryListing: {
                enable: true,
                path: 'build'
            },
            open: true
        } ) );
} );

gulp.task( 'watch', function() {
    gulp.watch( [ 'js/**/*.js', 'css/**/*.css', 'partials/**/*.html', 'img/**/*.{jpg,gif,png}', 'assets/**/*.*', 'fonts/**/*.*', 'index.html' ], [ 'clean-build', 'minify-html', 'minify-assets', 'minify-fonts', 'minify-img', 'browserify-dev' ] );
} );
