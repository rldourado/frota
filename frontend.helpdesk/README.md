# Configurações do CitSmart

### Ambientes

##### Homologação

URL: http://10.114.171.52:8080/citsmart

*Banco de Dados MySQL:*
```
host : 10.114.171.52
port : 5432
db   : citsmart_homo
user : citsmart
pass : citsmart
```

##### Produção

URL: https://www.mppe.mp.br/citsmart

### Pré-Requisitos

* Usuário com permissão de administrador

### Carga de WebServices

Menu: `Sistema -> Banco de Dados -> Carregar Scripts WebService`

Selecionar idioma `Português` e clicar em `Carregar`

Confirmar a mensagem `Sobrescrever carga de dados?`

### Configurar WebServices

Adicionar permissões do grupo `solicitantes (id 21)` para os seguintes WebServices:
* service_listUnits
* data_query
* request_create
* request_getByUser
* request_getById
* request_list_occurrences

### Configurar ***data_query***

Efetuar os paços a seguir na tela de cadastro do WebService ***data_query***

##### Criação de Parâmetros

Usando o botão `Cadastrar novo parâmetro`, criar os seguintes parâmetros:

| Identificador               | Descrição                                                                          |
|-----------------------------|------------------------------------------------------------------------------------|
| SERVICE_CATALOG             | SQL recupera catálogo de serviços                                                  |
| SERVICES_BY_PARENT_AND_TYPE | SQL recupera serviços pelo serviço (Catálogo) pai e tipo (Incidente ou Requisição) |
| SERVICES_BY_PARENT          | Serviços-filhos por Serviço-Pai                                                    |
| REQUESTS_BY_USER_LOGIN      | SQL recupera solicitações/incidentes pelo login                                    |
| UNITS                       | SQL recupera unidades                                                              |
| LOCALS_BY_UNIT_ID           | SQL recupera localidades pelo id da unidade                                        |

##### Criação dos SELECT's

Usando o botão `Adicionar parâmetro`, cadastrar os seguintes SELECT's:

* Parâmetro **SQL recupera catálogo de serviços** (SERVICE_CATALOG):
```sql
SELECT s.idservico AS "id",
       s.nomeservico AS "name",
       cs.idcategoriaservico AS "categoryId",
       cs.nomecategoriaservico AS "categoryName",
       s.detalheservico AS "detail",
       s.objetivo AS "objective"
FROM servico s LEFT JOIN categoriaservico cs
  ON s.idcategoriaservico = cs.idcategoriaservico
WHERE s.tiposervico = 'N' AND s.idservico != 84
ORDER BY cs.idcategoriaservico ASC, s.idservico ASC
```

* Parâmetro **SQL recupera serviços pelo serviço (Catálogo) pai e tipo (Incidente ou Requisição)** (SERVICES_BY_PARENT_AND_TYPE):
```sql
SELECT idservico AS "id",
       nomeservico AS "name"
FROM servico
WHERE idservico IN ( SELECT idservico FROM servicoautorelacionamento WHERE idservicorelacionado = ${id} ) AND tiposervico = ${tipo}
```

* Parâmetro **Serviços-filhos por Serviço-Pai** (SERVICES_BY_PARENT):
```sql
SELECT idservico AS "id",
       nomeservico AS "name"
FROM servico
WHERE idservico IN ( SELECT idservico FROM servicoautorelacionamento WHERE idservicorelacionado = cast( ${id} AS BIGINT ) ) AND tiposervico = ${tipo}
```

* Parâmetro **SQL recupera solicitações/incidentes pelo login** (REQUESTS_BY_USER_LOGIN):
```sql
SELECT sol.idSolicitacaoServico AS "number",
       sol.dataHoraSolicitacao AS "startDateTime",
       sol.descricao AS "description",
       sol.idstatus AS "statusId",
       td.classificacao AS "type"
FROM solicitacaoservico sol INNER JOIN tipodemandaservico td ON td.idtipodemandaservico = sol.idtipodemandaservico
     LEFT JOIN usuario usu ON usu.idusuario = sol.idresponsavel
WHERE usu.login = ${login}
ORDER BY "startDateTime" DESC
```

* Parâmetro **SQL recupera unidades** (UNITS):
```sql
SELECT u.idunidade AS "id",
       u.nome AS "name"
FROM unidade u
WHERE u.idunidade <> 2
ORDER BY u.nome
```

* Parâmetro **SQL recupera localidades pelo id da unidade** (LOCALS_BY_UNIT_ID):
```sql
SELECT l.idlocalidade AS "id",
       l.nomelocalidade AS "name"
FROM localidade l INNER JOIN localidadeunidade lu ON l.idlocalidade = lu.idlocalidade
WHERE lu.idunidade = CAST( ${id} AS BIGINT )
ORDER BY l.nomelocalidade
```
